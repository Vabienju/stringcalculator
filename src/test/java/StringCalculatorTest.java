import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StringCalculatorTest {

    StringCalculator stringCalculator;

    @BeforeEach
    void init(){
        stringCalculator = new StringCalculator();
    }

    @Test
    @DisplayName("Escape entry")
    void test0() {
        int result = stringCalculator.Add(" ");
        assertEquals(0,result);
    }

    @Test
    @DisplayName("Empty entry")
    void test1() {
        int result = stringCalculator.Add("");
        assertEquals(0,result);
    }

    @Test
    @DisplayName("Only one number")
    void test2() {
        int result = stringCalculator.Add("2");
        assertEquals(2,result);
    }

    @Test
    @DisplayName("Two Number")
    void test3() {
        int result = stringCalculator.Add("1,2");
        assertEquals(3,result);
    }

    @Test
    @DisplayName("Negatif Number")
    void test4() {
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            stringCalculator.Add("-1,2");
        });
        String expectedMessage = "negatives not allowed : -1";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    @DisplayName("N Number")
    void test5() {
        int result = stringCalculator.Add("1,2,3,4");
        assertEquals(1+2+3+4,result);
    }

    @Test
    @DisplayName("String finish by delimiter")
    void test6() {
        int result = stringCalculator.Add("1,2,3,4,");
        assertEquals(1+2+3+4,result);
    }

    @Test
    @DisplayName("Two delimiter ")
    void test7() {
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            stringCalculator.Add("1,2,,3,4,");
        });
        String expectedMessage = "Impossible to convert";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    @DisplayName("Not an integer")
    void test8() {
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            stringCalculator.Add("1,2,a,4");
        });
        String expectedMessage = "Impossible to convert";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    @DisplayName("Two Number")
    void test9() {
        int result = stringCalculator.Add("1\n2");
        assertEquals(3,result);
    }

    @Test
    @DisplayName("Negatif Number")
    void test10() {
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            stringCalculator.Add("-1\n2");
        });
        String expectedMessage = "negatives not allowed : -1";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    @DisplayName("N Number with \\n")
    void test11() {
        int result = stringCalculator.Add("1,2\n3,4");
        assertEquals(1+2+3+4,result);
    }

    @Test
    @DisplayName("Delimiter .")
    void test12(){
        int result = stringCalculator.Add("//\\.\n1.2");
        assertEquals(3,result);
    }

    @Test
    @DisplayName("Delimiter a")
    void test13(){
        int result = stringCalculator.Add("//a\n1a2");
        assertEquals(3,result);
    }

    @Test
    @DisplayName("Delimiter with escape before")
    void test13bis(){
        int result = stringCalculator.Add("  //a\n1a2");
        assertEquals(3,result);
    }


    @Test
    @DisplayName("Delimiter ;")
    void test14(){
        int result = stringCalculator.Add("//;,\n1;,2");
        assertEquals(3,result);
    }

    @Test
    @DisplayName("Multiple negative Number")
    void test15()  {
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            stringCalculator.Add("-1,-2,-3");
        });
        String expectedMessage = "negatives not allowed : -1,-2,-3";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    @DisplayName("Multiple negative Number with custom delimiter")
    void test16()  {
        RuntimeException exception = assertThrows(RuntimeException.class, () -> {
            stringCalculator.Add("//;,\n-1;,-2;,-3");
        });
        String expectedMessage = "negatives not allowed : -1,-2,-3";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }


}
