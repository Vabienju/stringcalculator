import java.util.ArrayList;
import java.util.List;

public class StringCalculator {

    private List<String> negativeNumber = new ArrayList<>();

    /*
     * input : string input with format "//[delimiter]\n[numbers…]"
     * Function Add return the result of the sum of the number in the input string
     */
    public int Add(String input){
        // Remove all escape
        input = input.replaceAll(" ", "");
        // Test if it's an empty string
        if (input.isEmpty() )
            return 0;
        String delimiter = ",|\n";

        // Update the delimiter if there is a custom delimiter
        if(delimeterPresent(input)){
            // Get the first index of \n
            int end_index = input.indexOf("\n");
            // Get the custom delimiter
            delimiter = input.substring(2, end_index);
            // Remove the custom delimiter from the string
            input = input.substring(end_index+1);
        }

        // Create an array with the number
        String[] numbers = input.split(delimiter);

        int result = calculSum(numbers);


        if(negativeNumber.size() != 0){
            // Create the string if there negartive number
            String message = "negatives not allowed : ";
            for (int i = 0; i < negativeNumber.size()-1; i++) {
                message += negativeNumber.get(i) + ",";
            }
            message += negativeNumber.get(negativeNumber.size()-1);
            // Throw the error
            throw new RuntimeException(message);
        }

        return result;
    }

    /*
     * numbers : array of the number present in the input String
     * return sum of the array
     */
    private int calculSum(String[] numbers){
        int result = 0;
        for (String number: numbers) {
            result += convertStringinInt(number);
        }
        return result;
    }

    /*
     * input : string with no escape
     * return true if there a custom delimiter
     */
    private boolean delimeterPresent(String input){
        return input.startsWith("//");
    }

    /*
     * number : number to parse into an int
     * return an integer
     */
    private int convertStringinInt(String number){
        int convert;
        try {
            convert = Integer.parseInt(number);
        } catch (Exception e){
            throw new RuntimeException("Impossible to convert");
        }
        if (convert < 0){
            negativeNumber.add(number);
        }
        return convert;
    }

}
